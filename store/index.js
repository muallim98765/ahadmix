export const state = () => ({
  auth: false,
  menuList: [],
  contactsList: [],
  banner: null,
  screens: null,
  partners: null,
  mapList: null,
  about: null,
  catalogList: null,
});
export const getters = {
  menuList: (state) => {
    return state.menuList;
  },
  contactsList: (state) => {
    return state.contactsList;
  },
  banner: (state) => {
    return state.banner;
  },
  screens: (state) => {
    return state.screens;
  },
  partners: (state) => {
    return state.partners;
  },
  mapList: (state) => {
    return state.mapList;
  },
  about: (state) => {
    return state.about;
  },
  catalogList: (state) => {
    return state.catalogList;
  },
};

export const mutations = {
  setState(state, { key, payload }) {
    state[key] = payload;
  },
};
export const actions = {
  fetchListDefault({ commit }, { api, key, params }) {
    return new Promise(async (resolve, reject) => {
      try {
        const res = await this.$axios.get(`/${api}`, { params });
        const { status, data, message } = res.data;
        if (status) {
          commit("setState", {
            key,
            payload: data.result || data || res || [],
          });
          resolve({
            status,
            data,
          });
        } else {
          reject(message);
        }
      } catch (error) {
        reject(error);
      }
    });
  },
};
