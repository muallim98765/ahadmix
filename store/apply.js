export const state = () => ({});
export const getters = {};

export const mutations = {
  setState(state, { key, payload }) {
    state[key] = payload;
  },
};
export const actions = {
  postForm(_, form) {
    return new Promise(async (resolve, reject) => {
      try {
        const { data } = await this.$axios.post(`/site/form/create`, form);
        if (data && data.status) {
          resolve({ error: false, status: true });
        } else {
          reject({ error: true, status: false });
        }
      } catch (error) {
        reject({
          error: true,
          status: false,
        });
      }
    });
  },
};
